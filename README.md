# kuai_shou_auto_script
# 作者：chenyujing（微信号:15280006510）,欢迎交流
快手定时自动无人直播脚本(jupyter-python脚本)
  [脚本说明视频](http://https://www.bilibili.com/video/BV1SG411T7we/)
## 一、环境安装
#### 1.安装windows Anaconda
       安装包： 链接：https://pan.baidu.com/s/1hliB8LFKxewNs8SurfZPHQ 
                   提取码：z7b7
       默认安装即可
#### 2.使用管理员权限打开 Anaconda 的Prompt 命令行
![Prompt 命令行](%E6%8D%95%E8%8E%B71.PNG)
#### 3.在命令行里安装需要的安装包
      python -m pip install --upgrade pip==20.3.4
      pip install pyautogui
      pip install schedule
![输入图片说明](install_pip.PNG)
#### 4. 启动jupyter服务
     在我们脚本的当前目录下(cd C:\Users\chenyujing\kuai_shou_auto_script),运行jupyter服务： 
     jupyter notebook --ip=0.0.0.0 --no-browser --allow-root --port 18001
![输入图片说明](jupyter.PNG) 
#### 5.在浏览器中输入jupyter的服务地址，并打开我们的脚本
     http://127.0.0.1:18001/?token=c16c7945db7a6f2cec56b9df276d3b043fc8e2c636bead35
![输入图片说明](web_jupyter.PNG)
## 二、使用脚本前的准备工作：
#### 1.打开快手直播程序
       最大化后，最小化在任务栏中，且放在任务栏中的第一个
#### 2.打开OBS程序
       最大化后，最小化在任务栏中，且放在任务栏中的第二个
    且确保：（1）处于“启动虚拟摄像机”模式；
           （2）视频选择好了；
           （3）是有显示“播放”“停止”按钮模式（方法是：在播放的视频处点击一下），
                且处于“停止”状态（要点下停止按钮）
#### 3. 配置脚本中参数
      （1）每天开始直播与结束直播的时间
      （2）配置快手程序个数 
![输入图片说明](%E9%85%8D%E7%BD%AE%E7%9A%84%E4%B8%A4%E4%B8%AA%E5%8F%82%E6%95%B0.PNG)
      
#### 4. 配置完时间后，运行此脚本：Cell -> Run All 
     运行后，把浏览器最小化就好（切记，不要关注这个浏览器页面”；
      也不要关闭“管理员：Anaconda Prompt”这个命令窗口）
#### 5. 注意：如果脚本一旦运行起来，想修改配置数据，需要先将程序停止，然后再启动脚本 
#### 6. 停止脚本的方法：Kernel -> Restart
